﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using NUnit.Framework;
using System.Linq;
using System.Threading;

public class Containerfiller
{
	public string ContainerFillerName { get; private set; }
	public List<Packet> PacketsToSort { get; private set;}
	private List<ContainerInformation> containerInformations = new List<ContainerInformation> ();

	public Dictionary<int,RotationInformation> rotationalInformations = new Dictionary<int, RotationInformation> ();
	public Dictionary<int,IPacketDepositStyle> depositStyleInformations = new Dictionary<int, IPacketDepositStyle> ();

	public int ID{ get; set;}
	public static int nextID = 0;
	int generation;

	public Fitness Fitness{ get; set; }

	public Containerfiller(List<Packet> packetsToSort,  int populationCounter)
    {
		ID = Interlocked.Increment (ref nextID);
		generation = populationCounter;
		this.ContainerFillerName = "Gen"+generation+ "-C" + ID;
		this.PacketsToSort = new List<Packet> ();
		PacketsToSort.AddRange(packetsToSort);
    }
	public Containerfiller(Containerfiller setupContainerFiller){
		generation = setupContainerFiller.generation+1;
		//Zeile ändern
		ID = Interlocked.Increment (ref nextID);

		this.ContainerFillerName = GenerateName (setupContainerFiller.ContainerFillerName);
		this.PacketsToSort = new List<Packet> ();
		PacketsToSort.AddRange(setupContainerFiller.PacketsToSort);
		rotationalInformations = new Dictionary<int, RotationInformation> (setupContainerFiller.rotationalInformations);
		depositStyleInformations = new Dictionary<int, IPacketDepositStyle> (setupContainerFiller.depositStyleInformations);
	}

	private string GenerateName(string predecessor){
		string returnS;
		//
		int index = predecessor.IndexOf('C');
		returnS =  "Gen"+generation+"-C" + ID + "-"+predecessor.Substring(index+1);

		return returnS;
	}

	public void ResetID(){
		nextID = 0;
	}

	public void Randomize(){
		RandomizePacketOrder ();
		RandomizeRotation ();
		RandomizeDepositStyles ();
	}

    /// <summary>
    /// Randomizes the packetOrder for the intial setup
    /// </summary>
	private void RandomizePacketOrder() {
        int n = PacketsToSort.Count;
        List<Packet> tempPackets = new List<Packet>();
        while (n > 0) {
            n--;
            int k = Random.Range(0,n + 1);
			tempPackets.Add(PacketsToSort[k]);
            PacketsToSort.RemoveAt(k);
        }
        PacketsToSort = tempPackets;
    }
	private void RandomizeRotation (){
		rotationalInformations = new Dictionary<int, RotationInformation> ();
		if (PacketsToSort != null) {
			for (int i = 0; i < PacketsToSort.Count; i++) {
				rotationalInformations.Add (PacketsToSort[i].ID, new RotationInformation (PacketsToSort[i],true));
			}
		}
	}

	private void RandomizeDepositStyles(){
		depositStyleInformations = new Dictionary<int, IPacketDepositStyle> ();
		if (PacketsToSort != null) {
			for (int i = 0; i < PacketsToSort.Count; i++) {
				depositStyleInformations.Add (PacketsToSort[i].ID, RandomDepositStyle(PacketsToSort[i]));
			}
		}
	}

    public void WritePacketOrder()
    {
        string packetOrder = "";
        foreach (var VARIABLE in PacketsToSort)
        {
            packetOrder += VARIABLE.Name;
        }
        Debug.Log(this.ContainerFillerName+": "+packetOrder);
    }

	public void SolveProblem()
    {
		foreach (var container in EvolutionManager.instance.Containers) {
			containerInformations.Add (new ContainerInformation (container.ID));
		}
		ResetToInitialState ();
		//Apply Rotation
		ApplySpecificPacketConditions();
		//Solve
		for (int i = 0; i < PacketsToSort.Count; i++) {
			InsertPacket (PacketsToSort [i]);
		}	
		//CalculateFitness
		int containerCount = 0;
		foreach (var container in EvolutionManager.instance.Containers) {
			if (container.gameObject.activeSelf) {
				containerCount++;
			}
		}
		Fitness = new Fitness(containerCount, CalculateUnusedSurface());
		EvolutionManager.instance.DebugStatement ("Container: " + Fitness.ContainerCount + Environment.NewLine + "Unused surface: " + Fitness.UnusedSurface);
		//EnableMyself (false);
		//yield return null;
    }
	/// <summary>
	/// Reset initial packetPosition, rotation and containers
	/// </summary>
	private void ResetToInitialState(){
		foreach (var container in EvolutionManager.instance.Containers) {
			container.gameObject.SetActive (false);
		}
		foreach (var packet in PacketsToSort) {
			packet.transform.position = new Vector3 (5, 0, 0);
			packet.ResetRotation ();
		}
	}

	/// <summary>
	/// Applies the specific packet conditions (Rotation, PacketDepositStyle)
	/// </summary>
	private void ApplySpecificPacketConditions(){
		foreach (var item in rotationalInformations) {
			item.Value.ApplyRotations ();
		}
		foreach (var item in depositStyleInformations) {
			PacketsToSort.Find (x => x.ID == item.Key).DepositStyle = item.Value;
		}
	}

	public void EnableMyself(bool enable){
		ResetToInitialState ();
		if (enable) {
			ApplySpecificPacketConditions ();
			for (int i = 0; i < Fitness.ContainerCount; i++) {
				EvolutionManager.instance.Containers [i].gameObject.SetActive (true);
				PositionPackets (i); 
			}
			Debug.Log ("Container "+ContainerFillerName+": " + Environment.NewLine + "Fitness: "+ Fitness.ContainerCount +" / " + Fitness.UnusedSurface);
		}
	}
	private void PositionPackets(int containerID){		
		foreach (var packet in containerInformations[containerID].PacketInformations) {
			PacketsToSort.Find (x => x.ID == packet.PacketID).transform.position = packet.Position;
		}
	}

	/// <summary>
	/// Inserts the packet into any existing containers. Generates a new container if that doesnt work
	/// </summary>
	/// <param name="packet">Packet.</param>
	private void InsertPacket(Packet packet){
		int currentContainerID = 0;

		foreach (var container in EvolutionManager.instance.Containers) {
			container.gameObject.SetActive (true);
			if (container.InsertPacket (packet, containerInformations[currentContainerID])) {
				PutPacketInformation (packet, currentContainerID);
				return;
			}
			currentContainerID++;
		}
		EvolutionManager.instance.AddContainer ();
		EvolutionManager.instance.Containers [EvolutionManager.instance.Containers.Count - 1].gameObject.SetActive (true);
		containerInformations.Add (new ContainerInformation (EvolutionManager.instance.Containers.Count-1));
		EvolutionManager.instance.Containers [EvolutionManager.instance.Containers.Count - 1].InsertPacket (packet, containerInformations[currentContainerID]);
		PutPacketInformation (packet, currentContainerID);
	}

	private void PutPacketInformation(Packet packet, int containerID){
		containerInformations.Find (x => x.ContainerID == containerID).AddPacket (packet);
	}

	public float CalculateUnusedSurface(){
		float unusedSurface = 0;
		foreach (var packet in PacketsToSort) {
			unusedSurface += packet.CalculateUnusedSurface ();
		}
		return unusedSurface;
	}

	//Containerfillerevolution
	public void Evolve(){
		float orderReversal = Random.Range (0f, 100f);
		int amountOfOrderChanges = Random.Range (0, 5);
		int amountOfRotationChanges = Random.Range (0, 5);
		int amountOfDepositStyleChanges = Random.Range (0, 5);

		if (orderReversal <= 0.25f) {
			PacketsToSort.Reverse ();
		}

		for (int i = 0; i < amountOfOrderChanges; i++) {
			RandomlyChangeOrder ();
		}
		for (int i = 0; i < amountOfRotationChanges; i++) {
			RandomlyChangeRotation ();
		}
		for (int i = 0; i < amountOfDepositStyleChanges; i++) {
			RandomlyChangeDepositStyle ();
		}
	}
	private void RandomlyChangeOrder(){
		int swap1, swap2;
		swap1 = Random.Range (0, PacketsToSort.Count - 1);
		do {
			swap2 = Random.Range (0, PacketsToSort.Count - 1);
		} while (swap1 == swap2);

		Packet tempPacket = PacketsToSort [swap2];
		PacketsToSort [swap2] = PacketsToSort [swap1];
		PacketsToSort [swap1] = tempPacket;
	}

	private void RandomlyChangeRotation(){
		int position = Random.Range (0, rotationalInformations.Count - 1);
		rotationalInformations [position].RandomlyChangeRotation ();
	}

	private void RandomlyChangeDepositStyle(){
		int position = Random.Range (0, depositStyleInformations.Count - 1);
		depositStyleInformations [position] = RandomDepositStyle (PacketsToSort.Find (x => x.ID == position));
	}

	private IPacketDepositStyle RandomDepositStyle(Packet packet){
		int decider = Random.Range(0,2);
		switch (decider) {
		case 0:
			return new FrontUp (packet);
		case 1:
			return new UpFront (packet);
		case 2:
			return new RightFrontUp (packet);
		case 3:
			return new RightUpFront (packet);
		case 4:
			return new FrontRightUp (packet);
		case 5:
			return new UpRightFront (packet);
		default:
			return new FrontUp (packet);
		}
	}
}
