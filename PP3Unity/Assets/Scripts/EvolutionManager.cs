﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using System.Linq;
using System;

public class EvolutionManager : MonoBehaviour
{

	public static EvolutionManager instance = null; 
	public bool debugMode = false;

	int terminationCounter = 0;

	private List<Population> populations = new List<Population>();
	private List<Packet> packets = new List<Packet>();
	private List<Container> containers = new List<Container>();

	private Containerfiller currentShownContainerfiller;

	public int amountOfPackages = 50;
	public int amountOfContainerfiller = 25;

	bool currentlySolving = false;
	bool currentlySolved = false;
	bool waitingForEvolution = false;
	int evolutionToDoCounter = 0;

	public GameObject packetPrefab;
	public GameObject containerPrefab;
	public float ContainerInsideLength{get;private set; }
	public float ContainerInsideHeigth{get;private set; }
	public float ContainerInsideWidth{get;private set; }

	int populationCounter, containerfillerCounter;

	void Awake()
	{
		//Check if instance already exists
		if (instance == null)

			//if not, set instance to this
			instance = this;

		//If instance already exists and it's not this:
		else if (instance != this){
			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy (gameObject);  
		}  
		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad (gameObject);
	}

	// Use this for initialization
	void Start () {
		containers = new List<Container>();
		AddContainer ();

		ContainerInsideLength = containers[0].gameObject.transform.FindChild ("Basis").localScale.x - 2* containers[0].gameObject.transform.FindChild("Rearwall").localScale.x;
		ContainerInsideHeigth = containers[0].gameObject.transform.FindChild ("Rearwall").localScale.y - 2* containers[0].gameObject.transform.FindChild("Basis").localScale.y;
		ContainerInsideWidth = containers[0].gameObject.transform.FindChild ("Rearwall").localScale.z - 2* containers[0].gameObject.transform.FindChild("Rearwall").localScale.x;

		populations = new List<Population>();
		packets =new List<Packet>();
		//packets = GenerateRandomPackets ();
		packets = GenerateComparablePackets();
		SetupBasePopulation ();

		populationCounter = 0;
		containerfillerCounter = 0;
	}

	// Update is called once per frame
	void Update () {	
		/*Inputs for:
		 * 
		 * 
		 * 
		 * 
		 */
		//1x Solve and Evolve 
		if (Input.GetKeyDown (KeyCode.Alpha1) && !currentlySolved) {
			containerfillerCounter = 0;
			currentlySolving = true;
			waitingForEvolution = true;
			evolutionToDoCounter++;
		}
		//5x Solve and Evolve
		if (Input.GetKeyDown (KeyCode.Alpha2) && !currentlySolved) {
			containerfillerCounter = 0;
			currentlySolving = true;
			waitingForEvolution = true;
			evolutionToDoCounter += 5;
		}
		//10x Solve and Evolve
		if (Input.GetKeyDown (KeyCode.Alpha3) && !currentlySolved) {
			containerfillerCounter = 0;
			currentlySolving = true;
			waitingForEvolution = true;
			evolutionToDoCounter += 10;
		}
		//20x Solve and Evolve
		if (Input.GetKeyDown (KeyCode.Alpha4) && !currentlySolved) {
			containerfillerCounter = 0;
			currentlySolving = true;
			waitingForEvolution = true;
			evolutionToDoCounter += 20;
		}
		//50x Solve and Evolve
		if (Input.GetKeyDown (KeyCode.Alpha5) && !currentlySolved) {
			containerfillerCounter = 0;
			currentlySolving = true;
			waitingForEvolution = true;
			evolutionToDoCounter += 50;
		}
		//100x Solve and Evolve
		if (Input.GetKeyDown (KeyCode.Alpha6) && !currentlySolved) {
			containerfillerCounter = 0;
			currentlySolving = true;
			waitingForEvolution = true;
			evolutionToDoCounter += 100;
		}
		//1000x Solve and Evolve
		if (Input.GetKeyDown (KeyCode.Alpha7) && !currentlySolved) {
			containerfillerCounter = 0;
			currentlySolving = true;
			waitingForEvolution = true;
			evolutionToDoCounter += 1000;
		}

		//Solve all containerfiller
		if (Input.GetKeyDown (KeyCode.KeypadEnter) && !currentlySolved) {
			currentlySolving = true;
		}

		//Show fittest containerfiller of current generation
		if (Input.GetKeyDown (KeyCode.F) && !currentlySolving) {
			DisableCurrentShownContainerfiller ();
			populations [populations.Count - 2].SortByFitness ();
			populations [populations.Count - 2].containerfillers [0].EnableMyself (true);
			currentShownContainerfiller = populations [populations.Count - 2].containerfillers [0];
			containerfillerCounter = 0;
		}

		//Solve one containerfiller
		if (Input.GetKeyDown (KeyCode.I) && containerfillerCounter < populations[populations.Count - 1].containerfillers.Count) {
			DisableCurrentShownContainerfiller ();
			populations [populations.Count - 1].containerfillers[containerfillerCounter].SolveProblem ();
			currentShownContainerfiller = populations [populations.Count - 1].containerfillers[containerfillerCounter];
			containerfillerCounter++;
		}
		//Cycles and display all 
		if (Input.GetKeyDown (KeyCode.C) && !currentlySolving) {
			if (containerfillerCounter >= populations [populations.Count - 2].containerfillers.Count) {
				containerfillerCounter = 0;
			}
			DisableCurrentShownContainerfiller ();
			populations [populations.Count - 2].containerfillers [containerfillerCounter].EnableMyself (true);
			currentShownContainerfiller = populations [populations.Count - 2].containerfillers [containerfillerCounter];
			if (containerfillerCounter != populations [populations.Count - 2].containerfillers.Count - 2) {
				containerfillerCounter++;
			} else {
				containerfillerCounter = 0;
			}
		}

		//Apply Rotation
		if(Input.GetKeyDown(KeyCode.R)  && !currentlySolving){
			Debug.Log ("Applying Rotation");
			foreach (var item in currentShownContainerfiller.rotationalInformations) {
				item.Value.ApplyRotations ();
				Debug.Log ("Applying Rotation");
			}
		}
		//Remove Rotation
		if(Input.GetKeyDown(KeyCode.T)  && !currentlySolving){
			foreach (var packet in packets) {
				packet.ResetRotation ();
				Debug.Log ("Removing Rotation");
			}
		}
		//Log all Evolutions
		if(Input.GetKeyDown(KeyCode.O)){
			foreach (var population in populations) {
				LogEvolutionStats (population);
			}
		}

		if (currentlySolving) {			
			SolvePopulation (populations [populations.Count - 1]);
		}

	}

	public void DebugStatement(string s){
		if(debugMode){
			Debug.Log (s);
		}
	}

	private void SolvePopulation(Population population){
		int loopBreak = containerfillerCounter+10;
		for (int i = containerfillerCounter; i<loopBreak && i < population.containerfillers.Count; containerfillerCounter++, i++) {
			DisableCurrentShownContainerfiller ();
			population.containerfillers[i].SolveProblem ();
			currentShownContainerfiller = population.containerfillers[i];
		}
		if (containerfillerCounter >= population.containerfillers.Count) {
			currentlySolving = false;
			currentlySolved = true;
			populations [populations.Count - 1].CalculateAverageFitness ();
			if (waitingForEvolution) {
				DoEvolutionStep ();
				currentlySolved = false;
				waitingForEvolution = false;
				if (--evolutionToDoCounter != 0) {
					currentlySolving = true;
					waitingForEvolution = true;
				}
			}
		}
	}

	private void DisableCurrentShownContainerfiller(){
		if (currentShownContainerfiller != null) {
			currentShownContainerfiller.EnableMyself (false);
		}
	}
	private void SetupBasePopulation(){
		Population p = new Population (containerPrefab);
		p.GenerateBasePopulation (packets,amountOfContainerfiller);
		populations.Add (p);
	
	
	}
	private List<Packet> GenerateRandomPackets(){
		List<Packet> packets = new List<Packet>();
		for (int i = 0; i < amountOfPackages; i++) {
			packets.Add (GeneratePacket());
		}
		return packets;
	}
	private List<Packet> GenerateComparablePackets(){
		List<Packet> packets = new List<Packet>();


		for (int i = 0; i < 5; i++) {
			packets.Add (GeneratePacket(new Vector3 (1f, 0.6f, 0.6f)));
		}
		for (int i = 0; i < 5; i++) {
			packets.Add (GeneratePacket(new Vector3 (0.8f, 0.5f, 0.4f)));
		}
		//Fringe cases
		for (int i = 0; i < 2; i++) {
			packets.Add (GeneratePacket(new Vector3 (2f, 2f, 2f)));
			packets.Add (GeneratePacket(new Vector3 (6f, 0.4f, 1f)));
			packets.Add (GeneratePacket(new Vector3 (1f, 1f, 1f)));
			packets.Add (GeneratePacket(new Vector3 (0.9f, 2.2f, 0.2f)));
			packets.Add (GeneratePacket(new Vector3 (0.4f, 0.4f, 2.2f)));
			packets.Add (GeneratePacket(new Vector3 (0.2f, 0.3f, 0.4f)));
			packets.Add (GeneratePacket(new Vector3 (0.4f, 0.3f, 0.2f)));
		}

		//Add packets until the amount is reached
		for (float i = 0.1f; packets.Count <= amountOfPackages; i+= 0.01f) {
			packets.Add (GeneratePacket(new Vector3 (0.5f+2*i, 0.4f+i, 0.4f+i)));
			packets.Add (GeneratePacket(new Vector3 (0.5f+2*i, 0.4f+i, 0.4f+i)));
		}

		return packets;
	}
	private Packet GeneratePacket(Vector3 size){
		GameObject tempP = Instantiate (packetPrefab);
		Packet p = tempP.GetComponent<Packet> ();
		p.Setup ();
		p.SetPacketSize (size);
		return p;
	}
	private Packet GeneratePacket(){
		GameObject tempP = Instantiate (packetPrefab);
		Packet p = tempP.GetComponent<Packet> ();
		p.Setup ();
		p.RandomizePacketSize (0.1f, 2f, 0.1f, 2f, 0.1f, 2f);
		return p;
	}


    public void DoEvolutionStep()
    {
		//Check termination
		if (IsTerminationReached (populations [populations.Count - 1])) {
			Debug.Log ("Termination reached, i should do something");
		} else {

			//Reset relevant data
			containerfillerCounter = 0;

			LogEvolutionStats (populations [populations.Count - 1]);
			//Sort population by Fitness
			populations [populations.Count - 1].SortByFitness ();
			//Generate and evolve next gen
			populations.Add (new Population (GenerateNextGeneration (populations [populations.Count - 1].containerfillers), containerPrefab));
			populationCounter++;

			DebugStatement ("New evolution ready for takeoff");
		}
	}

	private bool IsTerminationReached(Population population){		
		int terminationAmount = 10;
		float avgSurface = 0;
		float avgContainer = 0;
		if (population.Id - terminationAmount < 0) {
			return false;
		} else {
			for (int i = population.Id-terminationAmount; i < population.Id; i++) {
				avgSurface += populations[i].AverageFitness.UnusedSurface;
				avgContainer += populations[i].AverageFitness.ContainerCount;
			}
		}
		avgSurface /= terminationAmount;
		avgContainer /= terminationAmount;
		if (population.AverageFitness.ContainerCount < (avgContainer - avgContainer / 200)) {
			Debug.Log ("Termination reset");
			terminationCounter = 0;
			return false;
		}
		if (population.AverageFitness.UnusedSurface < (avgSurface - avgSurface / 200)) {
			Debug.Log ("Termination reset");
			terminationCounter = 0;
			return false;
		}
		terminationCounter++;
		if (terminationCounter > terminationAmount) {
			Debug.Log ("GA terminated");
			return true;
		}

		return false;
	}

	private List<Containerfiller> GenerateNextGeneration(List<Containerfiller> containerfiller){
		//Reset ID's
		containerfiller[0].ResetID();

		List<Containerfiller> nextGeneration = new List<Containerfiller> ();
		//Top 10 (Elitism)
		float elitismpercentage = 0.1f;
		int elitismAmount = (int)(containerfiller.Count * elitismpercentage);
		for (int i = 0; i < elitismAmount; i++) {
			nextGeneration.Add (new Containerfiller(containerfiller[i]));
		}
//		//Choose from the remaining
//		while (nextGeneration.Count < containerfiller.Count) {
//			for (int i = elitismAmount; i < containerfiller.Count; i++) {
//				if (UnityEngine.Random.Range (0, containerfiller.Count) > i) {
//					nextGeneration.Add (new Containerfiller(containerfiller [i]));
//					nextGeneration.Add (new Containerfiller(containerfiller [i]));
//					if (nextGeneration.Count == containerfiller.Count) {
//						break;
//					}
//				}
//			}
//		}
		//Rangbasierte Auswahl (Emax=1.1)
		while (nextGeneration.Count < containerfiller.Count) {
			float selectionNumber = UnityEngine.Random.Range (0f, containerfiller.Count);
			for (int i = 0; i < containerfiller.Count; i++) {

				selectionNumber -= CalculateExpectation(i+1);//Erwartungswert
					if(selectionNumber <= 0){
						nextGeneration.Add(new Containerfiller(containerfiller[i]));
					}
			}
		}
		//Evolve chosen containerfiller
		for(int i = elitismAmount; i<nextGeneration.Count ;i++){
			nextGeneration [i].Evolve ();
		}
	
		return nextGeneration;
	}
	private float CalculateExpectation(int rank){
		float eMax = 1.2f;
		float eMin = 2f - eMax;
		float exp = eMin + (eMax - eMin) * (amountOfContainerfiller - rank - 1) / (amountOfContainerfiller - 1);

		return exp;
	}

	private void LogEvolutionStats(Population population){
		Debug.Log("Evolutionstage: "+population.Id + Environment.NewLine + "Average containercount: "+population.AverageFitness.ContainerCount +"\t Average open surface: "+population.AverageFitness.UnusedSurface);
	}

	public void AddContainer(){
		GameObject container = GameObject.Instantiate (containerPrefab);
		container.transform.position += new Vector3 (0f, 0f, containers.Count * 5f);
		container.gameObject.SetActive (false);
		containers.Add (container.GetComponent<Container>());
	}


	public Containerfiller CurrentShownContainerfiller {
		get {
			return currentShownContainerfiller;
		}
		set {
			currentShownContainerfiller = value;
		}
	}

	public List<Container> Containers {
		get {
			return containers;
		}
		set {
			containers = value;
		}
	}

	public List<Packet> Packets {
		get {
			return packets;
		}
		set {
			packets = value;
		}
	}
}
