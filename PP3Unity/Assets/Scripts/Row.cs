﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Row {

	public float LeftZ{ get; private set; }
	public List<Packet> containedPackets{ get; set; }

	public Row(float leftZ){		
		LeftZ = leftZ;

		containedPackets = new List<Packet> ();
	}
	public Row(Packet packet){
		LeftZ = packet.transform.position.z - packet.transform.localScale.z / 2;
		containedPackets = new List<Packet> ();
	}


}
