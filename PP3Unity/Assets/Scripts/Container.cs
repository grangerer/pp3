﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;




public class Container : MonoBehaviour
{
	public int ID{ get; set;}
	public static int nextID = -1;

	const float minimalGap = 0.001f;


	void Awake () {
		ID = Interlocked.Increment (ref nextID);
	}		

	public bool InsertPacket(Packet packet, ContainerInformation containerInformation){
		//Step 1: Go to initial position
		GoToBottomLeftEntryPosition(packet);
		return packet.DepositStyle.InsertPacket (this, containerInformation);
	}


	private void GoToBottomLeftEntryPosition(Packet packet){
		float xPosition = transform.position.x + EvolutionManager.instance.ContainerInsideLength/2 + packet.transform.localScale.x;
		float yPosition = transform.position.y + transform.FindChild("Basis").localScale.y/2 +packet.transform.localScale.y/2;
		float zPosition = transform.position.z - EvolutionManager.instance.ContainerInsideWidth/2 + packet.transform.localScale.z/2;
		Vector3 initialPosition = new Vector3(xPosition,yPosition,zPosition);
		packet.transform.position = initialPosition;
	}

}

