﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacketDepositStyle {

	const float minimalGap = 0.001f;
	protected Packet packet;
	protected GameObject container;

	public PacketDepositStyle(Packet packet){		
		this.packet = packet;
	}

	protected bool AddNewRow(ContainerInformation containerInformation){

		Vector3 startPosition = new Vector3(packet.transform.position.x,packet.transform.position.y,packet.transform.position.z);
		GoToMinimumHeight ();
		GoToMinimumLength ();
		bool colliderHit = false;
		string oldHitName ="";
		try{
			while (true) {
				RaycastHit hit;
				Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (-1, 0), out hit);
				if (hit.collider != null) {
					if (hit.collider.transform.name.Equals ("Rearwall")) {

						if(containerInformation.Rows.Count==0){
							packet.transform.position -= new Vector3(0,0,minimalGap);
						}
						//Add the row
						containerInformation.Rows.Add(new Row(packet));
						float leftZ = containerInformation.Rows[containerInformation.Rows.Count-1].LeftZ;
						//EvolutionManager.instance.DebugStatement ("New row added "+ packet.transform.position.z);
						colliderHit = true;
						break;
					} else if (packet.transform.localScale.z <= CalculateDistanceToRightWall (hit.transform)) {
						float newZPosition = hit.collider.transform.position.z + hit.collider.transform.localScale.z / 2 + packet.transform.localScale.z / 2 + minimalGap;
						packet.transform.position = new Vector3 (packet.transform.position.x, packet.transform.position.y, newZPosition);
						//EvolutionManager.instance.DebugStatement ("New right position " + packet.Name + "|" + newZPosition + "|" + hit.collider.transform.name);
						if(hit.transform.name.Equals(oldHitName)){
							Debug.LogError ("Loop in AddNewRow");
							break;
						}
						oldHitName = hit.transform.name;
					} else {
						//EvolutionManager.instance.DebugStatement ("No new row due to Distance to Rightwall: " + packet.Name + "|" + packet.transform.position + "|" + CalculateDistanceToRightWall (hit.transform) + "|" + packet.transform.localScale.z);
						colliderHit = false;
						break;
					}
				} else {
					//EvolutionManager.instance.DebugStatement ("No new row due to collider was hit" + packet.Name + "|" + packet.transform.position);
					colliderHit = false;
					break;
				}
			}
			return colliderHit;
		}finally{
			packet.transform.position = startPosition;
		}
	}

	protected bool InsertToFront(Row row){
		//Adjust z position		
		Vector3 startPosition = new Vector3(packet.transform.position.x,packet.transform.position.y,packet.transform.position.z);
		GoToRow(row);
		GoToMinimumHeight ();
		RaycastHit hit;
		Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (-1, 0),out hit);
		if (hit.collider != null) {
			//Check if the packet fits in front of the collider
			if (packet.transform.localScale.x <= CalculateDistanceToContainerEntry (hit.transform)) {
				packet.transform.position = new Vector3 (hit.transform.position.x + hit.transform.localScale.x / 2 + packet.transform.localScale.x / 2, packet.transform.position.y, packet.transform.position.z);	
				//EvolutionManager.instance.DebugStatement ("InsertToFront, in front of: " + hit.transform.name + ". I am: " + packet.transform.name);
				row.containedPackets.Add (packet);
				return true;
			}
			//EvolutionManager.instance.DebugStatement ("Can't fit in front of: " + hit.transform.name + ". I am: " + packet.transform.name);
		}
		return false;
	}

	protected bool InsertToFrontUpwards(Row row){	
		Vector3 oldPosition = new Vector3(packet.transform.position.x, packet.transform.position.y, packet.transform.position.z);
		GoToRow(row);
		GoToMaximumHeight ();

		RaycastHit hit;
		Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (-1, 0),out hit);

		if (hit.collider == null) {
			packet.transform.position = oldPosition;
			return false;
		}
		//EvolutionManager.instance.DebugStatement ("Hitting "+hit.transform.name +" "+packet.transform.position.z+" "+ packet.name);
		if(CalculateDistanceToContainerEntry(hit.transform)<packet.transform.localScale.x){
			//EvolutionManager.instance.DebugStatement ("Not enough space, hitting "+hit.transform.name +""+row.LeftZ);
			return false;
		}
		float xPosition = hit.transform.position.x + (hit.transform.localScale.x+packet.transform.localScale.x)/2;
		packet.transform.position = new Vector3(xPosition, packet.transform.position.y, packet.transform.position.z);
		int i = 0;
		GameObject objectBelow = GetObjectBelowLeftRear (packet);
		while (true) {
			if (i++ > 20) {
				Debug.LogError ("Loop in InsertToFrontUpwards "+ xPosition + " Paketsize " +packet.transform.localScale.x);		
				break;
			}
			if (CheckOnlyOnePacketBelow()) {
				//Drop down and break out of loop
				DropDown();
				return true;
			}//Adjust Position and try again
			else {				
				objectBelow = GetObjectBelowLeftRear (packet);
				if (objectBelow == null) {
					//EvolutionManager.instance.DebugStatement ("No Object below. "+i +" I am " + packet.name);
					break;
				}
				xPosition = objectBelow.transform.position.x + (objectBelow.transform.localScale.x + packet.transform.localScale.x) / 2;
				packet.transform.position = new Vector3 (xPosition, packet.transform.position.y, packet.transform.position.z);
				if (CalculateDistanceToContainerEntry(packet.transform) < 0) {
					//EvolutionManager.instance.DebugStatement ("Not enough space. I am " + packet.name);
					break;
				}
				if(objectBelow.Equals(GetObjectBelow(packet))){
					packet.transform.position += new Vector3(0.01f, 0, 0);
				}
			
			}
		}
		packet.transform.position = oldPosition;
		return false;
	}

	protected bool NewInsertToFrontFromLeft(){
		//Adjust z position		
		Vector3 startPosition = new Vector3(packet.transform.position.x,packet.transform.position.y,packet.transform.position.z);
		GoToLeftSide ();
		GoToMinimumHeight ();
		int c = 0;
		RaycastHit hit;
		Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (-1, 0),out hit);
		Vector3 optimalPosition = startPosition;
		while (hit.collider != null) {
			//Check if the packet fits in front of the collider
			if (packet.transform.localScale.x <= CalculateDistanceToContainerEntry (hit.transform)) {
				Vector3 newPos = new Vector3 (hit.transform.position.x + hit.transform.localScale.x / 2 + packet.transform.localScale.x / 2, packet.transform.position.y, packet.transform.position.z);				 
				if(optimalPosition.x > newPos.x){
					optimalPosition = newPos;
				}
			}
			packet.transform.position = new Vector3 (packet.transform.position.x, packet.transform.position.y, hit.transform.position.z + hit.transform.localScale.z / 2 + packet.transform.localScale.z / 2 + minimalGap);
			Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (-1, 0),out hit);
			if (packet.CuboidMaxZ() > container.transform.position.z + EvolutionManager.instance.ContainerInsideWidth/2) {
				break;
			} if (++c > 20) {
				EvolutionManager.instance.DebugStatement ("Break Insert");
				break;
			}
		}
		if (!optimalPosition.Equals(startPosition)) {
			packet.transform.position = optimalPosition;
			return true;
		}
		packet.transform.position = startPosition;
		return false;
	}

	protected bool NewInsertToFrontFromRight(){
		//Adjust z position		
		Vector3 startPosition = new Vector3(packet.transform.position.x,packet.transform.position.y,packet.transform.position.z);
		GoToRightSide ();
		GoToMinimumHeight ();
		int c = 0;
		RaycastHit hit;
		Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (-1, 0),out hit);
		Vector3 optimalPosition = startPosition;
		while (hit.collider != null) {
			//Check if the packet fits in front of the collider
			if (packet.transform.localScale.x <= CalculateDistanceToContainerEntry (hit.transform)) {
				Vector3 newPos = new Vector3 (hit.transform.position.x + hit.transform.localScale.x / 2 + packet.transform.localScale.x / 2, packet.transform.position.y, packet.transform.position.z);
				if(optimalPosition.x > newPos.x){
					optimalPosition = newPos;
				}
			}
			packet.transform.position = new Vector3 (packet.transform.position.x, packet.transform.position.y, hit.transform.position.z - hit.transform.localScale.z / 2 - packet.transform.localScale.z / 2 - minimalGap);
			Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (-1, 0),out hit);
			if (++c > 20 || packet.transform.position.z < container.transform.position.z - EvolutionManager.instance.ContainerInsideWidth/2 + packet.transform.localScale.z/2) {
				EvolutionManager.instance.DebugStatement ("Break insert");
				break;
			}
		}
		if (!optimalPosition.Equals(startPosition)) {
			packet.transform.position = optimalPosition;
			return true;
		}
		packet.transform.position = startPosition;
		return false;
	}

	protected bool NewInsertUpwards(ContainerInformation containerInformation){
		Vector3 startPosition = new Vector3(packet.transform.position.x,packet.transform.position.y,packet.transform.position.z);
		GoAboveContainer ();
		containerInformation.SortPacketsByXPosition ();
		RaycastHit hit;
		foreach (var item in containerInformation.PacketInformations) {
			if (item.UnusedTopSurface < packet.transform.localScale.x * packet.transform.localScale.z) {
				continue;
			}
			Packet itemPacket = item.FindCorrespondingPacket ();
			if (EvolutionManager.instance.ContainerInsideHeigth - itemPacket.CuboidMaxY() > packet.transform.localScale.y) {
				PutAbovePacket (itemPacket);
				Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (0, -1),out hit);
				if(hit.distance > packet.transform.localScale.y && CheckOnlyOnePacketBelow ()) {
					DropDown ();
					return true;
				}
			}
			//X-Path
			int counter = 0;
			Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (0, -1),out hit);
			RaycastHit checkHit = new RaycastHit();
			while(hit.collider != null && !hit.collider.name.Equals(itemPacket.name)){
				if (packet.CuboidMaxX () < itemPacket.CuboidMaxX ()) {
					//EvolutionManager.instance.DebugStatement ("Break CuboidMaxX NewInsertUpwards X-Path");
					break;
				}
				if (hit.distance > packet.transform.localScale.y && CheckOnlyOnePacketBelow ()) {
					DropDown ();
					return true;
				}
				packet.transform.position = new Vector3(hit.collider.transform.position.x + hit.collider.transform.localScale.x/2 + packet.transform.localScale.x/2+minimalGap,packet.transform.position.y,packet.transform.position.z);
				Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (0, -1),out hit);
				if (checkHit.collider != null && checkHit.collider.Equals (hit.collider)) {
					EvolutionManager.instance.DebugStatement ("Same object NewInsertUpwards X-Path Counter: "+counter);
					break;
				}
				checkHit = hit;
				if (++counter > 6 ) {
					EvolutionManager.instance.DebugStatement ("CounterBreak NewInsertUpwards X-Path");
					break;
				}
			}
//			PutAbovePacket(itemPacket);
//			//Z-Path
//			counter = 0;
//			Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (0, -1),out hit);
//			while(hit.collider != null && (!hit.collider.name.Equals(itemPacket.name) || packet.transform.position.z + packet.transform.localScale.z/2 > itemPacket.transform.position.z + itemPacket.transform.localScale.z/2)){
//				if (CheckOnlyOnePacketBelow ()) {
//					DropDown ();
//					return true;
//				}
//				packet.transform.position = new Vector3(packet.transform.position.x, packet.transform.position.y, hit.collider.transform.position.z + (hit.collider.transform.localScale.z + packet.transform.localScale.z)/2);
//				Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (0, -1),out hit);
//				if (++counter > 20) {
//					EvolutionManager.instance.DebugStatement ("Break NewInsertUpwards Z-Path");
//					break;
//				}
//			}
		}
		packet.transform.position = startPosition;
		return false;
	}

	private void PutAbovePacket(Packet bottomPacket){
		float xPos = bottomPacket.CuboidMinX() + packet.transform.localScale.x / 2;
		float zPos = bottomPacket.CuboidMinZ() + packet.transform.localScale.z / 2;
		packet.transform.position = new Vector3(xPos, packet.transform.position.y, zPos);
	}
//	private bool InsertToRightUpwards(Packet packet){
//		Vector3 oldPosition = new Vector3(packet.transform.position.x,packet.transform.position.y,packet.transform.position.z);
//		//Step 1: Go to initial position
//		float yPosition = packet.transform.position.y + InsideHeigth-packet.transform.localScale.y + transform.FindChild ("Top").localScale.y;
//		packet.transform.position = new Vector3(packet.transform.position.x ,yPosition,packet.transform.position.z );
//
//		RaycastHit hit;
//		Physics.BoxCast (packet.transform.position, packet.GetHalfExtents (), new Vector3 (-1, 0),out hit);
//		if (hit.collider == null) {
//			packet.transform.position = oldPosition;
//			return false;
//		}
//		if (hit.collider.transform.name.Equals ("Rearwall")) {
//			float xPosition = packet.transform.position.x - hit.distance;
//			packet.transform.position = new Vector3 (xPosition, packet.transform.position.y, packet.transform.position.z);
//		}
//		if(CalculateDistanceToRightWall(hit.transform)<packet.transform.localScale.x){return false;}
//
//		while (true) {
//			if (CheckOnlyOnePacketBelow (packet)) {
//				//Drop down and break out of loop
//				DropDown(packet);
//				return true;
//			}//Adjust Position and try again
//			else {
//				GameObject hitLeftRear = RaycastDown (packet.GetBottomLeftRearCorner ());
//				if (hitLeftRear != null) {
//					packet.transform.position += new Vector3 (0, 0, hitLeftRear.transform.localScale.z);
//					if (CalculateDistanceToContainerEntry (packet.transform) < 0) {
//						packet.transform.position = oldPosition;
//						return false;
//					}
//				} else {
//					packet.transform.position = oldPosition;
//					return false;
//				}
//			}
//		}
//	}

	protected void DropDown(){
		//Raycast from each corner downwards
		GameObject hitLeftRear = RaycastDown(packet.GetBottomLeftRearCorner() + new Vector3(minimalGap,0,minimalGap));
		GameObject hitRightRear = RaycastDown (packet.GetBottomRightRearCorner () + new Vector3(minimalGap,0,-minimalGap));
		GameObject hitLeftFront = RaycastDown (packet.GetBottomLeftFrontCorner () + new Vector3(-minimalGap,0, minimalGap));
		GameObject hitRightFront = RaycastDown (packet.GetBottomRightFrontCorner () + new Vector3(-minimalGap,0,-minimalGap));
		float highestY = Mathf.Max (GetTopEnd(hitLeftRear),GetTopEnd(hitRightRear),GetTopEnd(hitLeftFront),GetTopEnd(hitRightFront));
		packet.transform.position = new Vector3(packet.transform.position.x, highestY+packet.transform.localScale.y/2,packet.transform.position.z);
		//EvolutionManager.instance.DebugStatement("Dropping down "+packet.Name+" at "+packet.transform.position);
		//Get all packets directly below and change their recalculate their UnusedTopSurface
	}

	protected GameObject RaycastDown(Vector3 startPosition){
		RaycastHit hit;
		if (Physics.Raycast(startPosition,new Vector3(0,-1),out hit)) {
			return hit.transform.gameObject;
		}
		return null;
	}
	protected float GetTopEnd(GameObject go){
		return go.transform.position.y + go.transform.localScale.y / 2;
	}
	protected bool CheckOnlyOnePacketBelow(){
		float maxDiff = 0.01f;
		//Get all corners
		Vector3 cornerLeftRear = packet.GetBottomLeftRearCorner () + new Vector3(minimalGap,0,minimalGap);
		Vector3 cornerRightRear = packet.GetBottomRightRearCorner () + new Vector3(minimalGap,0,-minimalGap);
		Vector3 cornerLeftFront = packet.GetBottomLeftFrontCorner () + new Vector3(-minimalGap,0, minimalGap);
		Vector3 cornerRightFront = packet.GetBottomRightFrontCorner () + new Vector3(-minimalGap,0,-minimalGap);
		Vector3 middle = packet.transform.position - new Vector3 (0, packet.transform.localScale.y / 2 + minimalGap, 0); 
		//Raycast from each corner downwards
		GameObject hitLeftRear = RaycastDown(cornerLeftRear);
		GameObject hitRightRear = RaycastDown (cornerRightRear);
		GameObject hitLeftFront = RaycastDown (cornerRightFront);
		GameObject hitRightFront = RaycastDown (cornerLeftFront);
		GameObject hitMiddle = RaycastDown (middle);
		if (hitLeftRear == null || hitRightRear == null || hitLeftFront == null || hitRightFront == null || hitMiddle == null) {
			return false;
		}
		//Check if raycasts hit the same target
		if (hitLeftRear.Equals (hitRightRear) && hitLeftRear.Equals (hitLeftFront) && hitLeftRear.Equals (hitRightFront) && hitLeftRear.Equals (hitMiddle)) {
			return true;
		} //Check if raycast hits have nearly the same height (variance determined based of maxDiff)
		else if(Mathf.Abs(Mathf.Min(GetTopEnd(hitMiddle),GetTopEnd(hitLeftRear),GetTopEnd(hitRightRear),GetTopEnd(hitLeftFront),GetTopEnd(hitRightFront))-Mathf.Max(GetTopEnd(hitMiddle),GetTopEnd(hitLeftRear),GetTopEnd(hitRightRear),GetTopEnd(hitLeftFront),GetTopEnd(hitRightFront)))<maxDiff ){
			//EvolutionManager.instance.DebugStatement ("Targets close together: " + GetTopEnd(hitLeftRear)+"|"+GetTopEnd(hitRightRear)+"|"+GetTopEnd(hitLeftFront)+"|"+GetTopEnd(hitRightFront));
			return true;
		}else{
			return false;
		}
	}

	private GameObject GetObjectBelow(Packet packet){
		RaycastHit hit;
		if (Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (0, -1), out hit)) {
			return hit.collider.gameObject;
		}
		return null;
	}
	private GameObject GetObjectBelowLeftRear(Packet packet){
		Vector3 cornerLeftRear = packet.GetBottomLeftRearCorner () + new Vector3(minimalGap,0,minimalGap);
		GameObject hitLeftRear = RaycastDown(cornerLeftRear);
		if (hitLeftRear != null) {
			return hitLeftRear;
		}
		return null;
	}

	//Calculations
	protected float CalculateDistanceToContainerEntry(Transform transformToCheck){
		if (transformToCheck.GetComponentInParent<Container>() != null) {			
			return EvolutionManager.instance.ContainerInsideLength;
		}
		float p1 = transformToCheck.position.x + transformToCheck.localScale.x / 2;
		float p2 = container.transform.position.x + EvolutionManager.instance.ContainerInsideLength / 2;
		float distance = p2 - p1;
		return distance;
	}

	protected float CalculateDistanceToRightWall(Transform transformToCheck){
		if (transformToCheck.GetComponentInParent<Container>() != null) {
			return EvolutionManager.instance.ContainerInsideWidth;
		}
		float p1 = transformToCheck.position.z + transformToCheck.localScale.z / 2;
		float p2 = container.transform.position.z + EvolutionManager.instance.ContainerInsideWidth / 2;
		float distance = (p1 -p2);
		return Mathf.Abs(distance);
	}
	protected float CalculateDistanceToRightWall(float leftZ){
		float p1 = leftZ;
		float p2 = container.transform.position.z + EvolutionManager.instance.ContainerInsideWidth / 2;
		float distance = (p1 -p2);
		return Mathf.Abs(distance);
	}


	protected void GoToRow(Row row){
		packet.transform.position = new Vector3(packet.transform.position.x,packet.transform.position.y,row.LeftZ+packet.transform.localScale.z/2);
	}

	protected void GoToLeftSide(){
		float zPosition = container.transform.position.z - EvolutionManager.instance.ContainerInsideWidth/2 + packet.transform.localScale.z/2;
		packet.transform.position = new Vector3(packet.transform.position.x, packet.transform.position.y, zPosition );
	}

	protected void GoToRightSide(){
		float zPosition = container.transform.position.z + EvolutionManager.instance.ContainerInsideWidth/2 - packet.transform.localScale.z/2;
		packet.transform.position = new Vector3(packet.transform.position.x, packet.transform.position.y, zPosition );
	}

	protected void GoToMaximumHeight(){
		float yPosition = container.transform.position.y + container.transform.FindChild("Basis").localScale.y/2 + EvolutionManager.instance.ContainerInsideHeigth-packet.transform.localScale.y/2;
		packet.transform.position = new Vector3(packet.transform.position.x ,yPosition,packet.transform.position.z );
	}
	protected void GoToMinimumHeight(){
		float yPosition = container.transform.position.y + container.transform.FindChild("Basis").localScale.y/2 +packet.transform.localScale.y/2;
		packet.transform.position = new Vector3(packet.transform.position.x ,yPosition,packet.transform.position.z );
	}
	protected void GoToMinimumLength(){
		float xPosition = container.transform.position.x + EvolutionManager.instance.ContainerInsideLength/2 + packet.transform.localScale.x;
		packet.transform.position = new Vector3(xPosition, packet.transform.position.y, packet.transform.position.z );
	}

	protected void GoAboveContainer(){
		float yPosition = container.transform.position.y + EvolutionManager.instance.ContainerInsideHeigth + container.gameObject.transform.FindChild("Basis").localScale.y + packet.transform.localScale.y/2;
		packet.transform.position = new Vector3(packet.transform.position.x, yPosition, packet.transform.position.z );
	}


}
