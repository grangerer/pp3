﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Linq;

public class Population {

	static int nextId = 0;
	public int Id {get; private set;}
	public List<Containerfiller> containerfillers { get; set; }
	GameObject containerPrefab;
	public Fitness AverageFitness;

	int containerfillerCounter = 0;

	public float maxLength, minLength, maxWidth, minWidth, maxHeight, minHeight;

	public Population(GameObject containerPrefab) {
		Id = Interlocked.Increment(ref nextId);
		containerfillers = new List<Containerfiller>();
		this.containerPrefab = containerPrefab;
	}
	public Population(List<Containerfiller> containerfillers, GameObject containerPrefab) {
		Id = Interlocked.Increment(ref nextId);
		this.containerfillers = new List<Containerfiller> ();
		this.containerfillers.AddRange(containerfillers);
		this.containerPrefab = containerPrefab;
	}


	public void SortByFitness(){
		containerfillers = containerfillers.OrderBy (x => x.Fitness.ContainerCount).ThenBy (x => x.Fitness.UnusedSurface).ToList ();	
	}


	public void GenerateBasePopulation(List<Packet> packets, int amountOfContainerfiller)
	{
		GenerateContainerfiller(amountOfContainerfiller, packets);
	}

	/// <summary>
	/// Generates containerfiller and randomizes their packetOrder
	/// </summary>
	/// <param name="amount"></param>
	public void GenerateContainerfiller(int amount, List<Packet> packets)
	{
		for (int i = 0; i < amount; i++) {
			Containerfiller c = new Containerfiller(packets, Id);
			c.Randomize ();
			c.WritePacketOrder ();
			containerfillers.Add(c);
		}
		//Reset ID
		containerfillers[0].ResetID();
	}

	public void CalculateAverageFitness(){
		float unusedSurfaceAverage;
		float usedContainerAverage;

		float unusedSurfaceSum = 0;
		float usedContainerSum = 0;
		foreach (var containerfiller in containerfillers) {
			unusedSurfaceSum += containerfiller.Fitness.UnusedSurface;
			usedContainerSum += containerfiller.Fitness.ContainerCount;
		}
		unusedSurfaceAverage = unusedSurfaceSum / containerfillers.Count;
		usedContainerAverage = usedContainerSum / containerfillers.Count;
		AverageFitness = new Fitness (usedContainerAverage, unusedSurfaceAverage);
	}

	public void LogPopulation(){
		foreach (Containerfiller containerfiller in containerfillers) {
			string debugStatement = "";
			foreach (var packet in containerfiller.PacketsToSort) {
				debugStatement += packet.name + "-";
			}
		}
	}

}
