﻿using System.Collections;
using System.Collections.Generic;

public interface IPacketDepositStyle {

	bool InsertPacket (Container container, ContainerInformation containerInformation);
}
