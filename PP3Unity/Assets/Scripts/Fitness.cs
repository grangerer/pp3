﻿using UnityEngine;
using System.Collections;

public class Fitness
{
	public float ContainerCount { get; private set; }

	public float UnusedSurface { get; private set; }

	public Fitness (float containerCount, float unusedSurface)
	{
		ContainerCount = containerCount;
		UnusedSurface = unusedSurface;
	}



}
