﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;

public class Cuboid : MonoBehaviour
{
	public float UnusedSurface{ get; protected set;}

	RotationInformation appliedRotation = null;

	protected static float gap = 0.001f;

	public float CalculateUnusedSurface(){
		//Check all directions
		float unusedSurface = CalculateSurface();
		//Debug.Log ("Unused surface before substraction: " +unusedSurface +" I am " +this.name);

		unusedSurface -= CalculateUsedSurfaceInDirection (new Vector3 (1, 0, 0));
		unusedSurface -= CalculateUsedSurfaceInDirection (new Vector3 (-1, 0, 0));
		unusedSurface -= CalculateUsedSurfaceInDirection (new Vector3 (0, 1, 0));
		unusedSurface -= CalculateUsedSurfaceInDirection (new Vector3 (0, -1, 0));
		unusedSurface -= CalculateUsedSurfaceInDirection (new Vector3 (0, 0, -1));
		unusedSurface -= CalculateUsedSurfaceInDirection (new Vector3 (0, 0, 1));

		UnusedSurface = unusedSurface;
		return UnusedSurface;
	}



	public float CalculateUsedSurfaceInDirection(Vector3 direction){
		float distance = 0.01f;
		float usedSurface = 0;

		RaycastHit[] hits = Physics.BoxCastAll (this.transform.position, this.GetBoxCastBox (), direction, Quaternion.identity, distance);
		foreach (var hit in hits) {
			if(!CheckIfHitIsInCorrectDirection(hit.transform.position,direction)){
				continue;
			}
			float t = new UsedSurface (hit.transform.gameObject, this).CalculateUsedSurface (direction);
			//Debug.Log("CalculateUsedSurfaceInDirection " + hit.transform.name +"|"+t);
			usedSurface += new UsedSurface (hit.transform.gameObject, this).CalculateUsedSurface(direction);
		}
		return usedSurface;
	}

	private bool CheckIfHitIsInCorrectDirection(Vector3 hitPosition, Vector3 direction){
		if (direction.x > 0) {
			if (hitPosition.x <= this.transform.position.x + this.transform.localScale.y/2) {
				return false;
			}
			return true;
		} else if (direction.x < 0) {
			if (hitPosition.x >= this.transform.position.x - this.transform.localScale.y/2) {
				return false;
			}
			return true;
		}else if (direction.y > 0) {
			if (hitPosition.y <= this.transform.position.y + this.transform.localScale.y/2) {
				return false;
			}
			return true;
		}else if (direction.y < 0) {
			if (hitPosition.y >= this.transform.position.y - this.transform.localScale.y/2) {
				return false;
			}
			return true;
		}else if (direction.z > 0) {
			if (hitPosition.z <= this.transform.position.z + this.transform.localScale.z/2) {
				return false;
			}
			return true;
		}else if (direction.z < 0) {
			if (hitPosition.z >= this.transform.position.z - this.transform.localScale.y/2) {
				return false;
			}
			return true;
		}
		Debug.LogError ("This shouldn't happen at CheckIfHitIsInCorrectDirection!");
		return false;
	}
			


	public Vector3 GetHalfExtents ()
	{
		return new Vector3 (transform.localScale.x / 2, transform.localScale.y / 2, transform.localScale.z / 2);
	}

	public Vector3 GetBoxCastBox ()
	{
		Vector3 returnVector = GetHalfExtents () - new Vector3 (gap, gap, gap);
		return returnVector;
	}

	public float CalculateSurface ()
	{
		return transform.localScale.x * transform.localScale.y *2 + transform.localScale.x * transform.localScale.z *2 + transform.localScale.y * transform.localScale.z *2;
	}		

	public void ResetRotation(){
		if (appliedRotation != null) {
			appliedRotation.DeapplyRotations ();
			appliedRotation = null;
		}
	}

	/// <summary>
	/// Rotates around 90° the x-axis.
	/// </summary>
	public void RotateAroundX ()
	{
		Vector3 temp = new Vector3 (transform.localScale.x,transform.localScale.z,transform.localScale.y);
		transform.localScale = temp;	
	}

	/// <summary>
	/// Rotates around 90° the y-axis.
	/// </summary>
	public void RotateAroundY ()
	{
		Vector3 temp = new Vector3 (transform.localScale.z,transform.localScale.y,transform.localScale.x);
		transform.localScale = temp;
	}

	/// <summary>
	/// Rotates around 90° the z-axis.
	/// </summary>
	public void RotateAroundZ ()
	{
		Vector3 temp = new Vector3 (transform.localScale.y,transform.localScale.x,transform.localScale.z);
		transform.localScale = temp;	
	}



	public float CuboidMinX () {return transform.position.x - transform.localScale.x / 2;}	
	public float CuboidMaxX () {return transform.position.x + transform.localScale.x / 2;}
	public float CuboidMinY () {return transform.position.y - transform.localScale.y / 2;}
	public float CuboidMaxY () {return transform.position.y + transform.localScale.y / 2;}			
	public float CuboidMinZ () {return transform.position.z - transform.localScale.z / 2;}
	public float CuboidMaxZ () {return transform.position.z + transform.localScale.z / 2;}


	public RotationInformation AppliedRotation {
		get {
			return appliedRotation;
		}
		set {
			appliedRotation = value;
		}
	}
}
