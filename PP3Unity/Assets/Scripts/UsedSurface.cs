﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UsedSurface
{

	float hitMinX;
	float hitMaxX;
	float hitMinY;
	float hitMaxY;
	float hitMinZ;
	float hitMaxZ;

	Cuboid cuboid;

	public UsedSurface (GameObject go, Cuboid cuboid)
	{
		hitMinX = go.transform.position.x - go.transform.localScale.x / 2;
		hitMaxX = go.transform.position.x + go.transform.localScale.x / 2;
		hitMinY = go.transform.position.y - go.transform.localScale.y / 2;
		hitMaxY = go.transform.position.y + go.transform.localScale.y / 2;
		hitMinZ = go.transform.position.z - go.transform.localScale.z / 2;
		hitMaxZ = go.transform.position.z + go.transform.localScale.z / 2;

		this.cuboid = cuboid;
	}

	public float CalculateUsedSurface(Vector3 direction)
	{
		if (direction.x != 0) {
			return (Mathf.Min (hitMaxY, cuboid.CuboidMaxY()) - Mathf.Max (hitMinY, cuboid.CuboidMinY())) * (Mathf.Min (hitMaxZ, cuboid.CuboidMaxZ()) - Mathf.Max (hitMinZ, cuboid.CuboidMinZ()));

		} else if (direction.y != 0) {
			return (Mathf.Min (hitMaxX, cuboid.CuboidMaxX()) - Mathf.Max (hitMinX, cuboid.CuboidMinX())) * (Mathf.Min (hitMaxZ, cuboid.CuboidMaxZ()) - Mathf.Max (hitMinZ, cuboid.CuboidMinZ()));

		} else {
			return (Mathf.Min (hitMaxY, cuboid.CuboidMaxY()) - Mathf.Max (hitMinY, cuboid.CuboidMinY())) * (Mathf.Min (hitMaxX, cuboid.CuboidMaxX()) - Mathf.Max (hitMinX, cuboid.CuboidMinX()));
		}
	}


}
