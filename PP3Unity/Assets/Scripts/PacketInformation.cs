﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacketInformation {

	int packetID;
	Vector3 position;
	float unusedTopSurface;
	bool rotatedAroundZ, rotatedAroundX, rotatedAroundY;

	public PacketInformation (Packet packet)
	{
		packetID = packet.ID;
		position = new Vector3(packet.transform.position.x,packet.transform.position.y,packet.transform.position.z);
		unusedTopSurface = packet.transform.localScale.x * packet.transform.localScale.z;
	}

	public Packet FindCorrespondingPacket(){
		return EvolutionManager.instance.Packets.Find (x => x.ID == this.packetID);
	}
	public void CalculateUnusedTopSurface(){
		Packet packet = FindCorrespondingPacket ();
		unusedTopSurface = packet.transform.localScale.x * packet.transform.localScale.z - packet.CalculateUsedSurfaceInDirection (new Vector3 (0, 1, 0));
	}

	public float UnusedTopSurface {
		get {
			return unusedTopSurface;
		}
		set {
			unusedTopSurface = value;
		}
	}

	public int PacketID {
		get {
			return packetID;
		}
		set {
			packetID = value;
		}
	}

	public Vector3 Position {
		get {
			return position;
		}
		set {
			position = value;
		}
	}

	public bool RotatedAroundZ {
		get {
			return rotatedAroundZ;
		}
		set {
			rotatedAroundZ = value;
		}
	}

	public bool RotatedAroundX {
		get {
			return rotatedAroundX;
		}
		set {
			rotatedAroundX = value;
		}
	}

	public bool RotatedAroundY {
		get {
			return rotatedAroundY;
		}
		set {
			rotatedAroundY = value;
		}
	}

}
