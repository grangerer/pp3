﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationInformation {

	List<Rotation> rotations;
	Packet packet;

	public RotationInformation(Packet packet, bool randomRotation = false){
		rotations = new List<Rotation> ();
		this.packet = packet;
		if (randomRotation) {
			SetRandomRotation ();
		}
	}
	public void SetRandomRotation(){
		int rng = Random.Range (0, 2);
		if (rng == 1) {
			AddRandomRotation ();
		}
	}

	public void AddRandomRotation(){
		if (CheckIsUnrotatablePacket ()) {
			Debug.Log ("CheckIsUnrotatablePacket");
			return;
		}

		bool rotationAdded = false;
		//improvised=>Refactor!!!
		while (!rotationAdded) {
			int rng = Random.Range (0, 3);
			switch (rng) {
			case 0:
				if (packet.transform.localScale.y < EvolutionManager.instance.ContainerInsideWidth && packet.transform.localScale.z < EvolutionManager.instance.ContainerInsideHeigth) {
					rotations.Add (new Rotation (true));
					rotationAdded = true;
				}
				break;
			case 1:
				if (packet.transform.localScale.x < EvolutionManager.instance.ContainerInsideWidth && packet.transform.localScale.z < EvolutionManager.instance.ContainerInsideLength) {
					rotations.Add (new Rotation (false, true));
					rotationAdded = true;
				}
				break;
			case 2:
				if (packet.transform.localScale.y < EvolutionManager.instance.ContainerInsideLength && packet.transform.localScale.x < EvolutionManager.instance.ContainerInsideHeigth) {
					rotations.Add (new Rotation (false, false, true));
					rotationAdded = true;
				}
				break;
			default:
				break;
			}
		}
	}

	private bool CheckIsUnrotatablePacket(){
		if (packet.transform.localScale.y > EvolutionManager.instance.ContainerInsideWidth && packet.transform.localScale.z > EvolutionManager.instance.ContainerInsideHeigth) {
			if (packet.transform.localScale.x > EvolutionManager.instance.ContainerInsideWidth && packet.transform.localScale.z > EvolutionManager.instance.ContainerInsideLength) {
				if (packet.transform.localScale.y > EvolutionManager.instance.ContainerInsideLength && packet.transform.localScale.x > EvolutionManager.instance.ContainerInsideHeigth) {
					Debug.Log ("Unrotatable Packet");
					return true;
				}
			}
		}
		return false;
	}

	public void RemoveRandomRotation(){
		int rngRemove = Random.Range(0,rotations.Count);
		rotations.RemoveAt (rngRemove);
	}

	//remove, reduce, recycle
	public void RandomlyChangeRotation(){
		//improvised=>Refactor!!!
		int rng;
		if (rotations.Count == 0) {
			rng = Random.Range (0, 1);
		} else if (rotations.Count != 3) {
			rng = Random.Range (0, 3);
		} else {
			rng = Random.Range (1, 3);
		}

		switch (rng) {
		case 0:	//Chance to add a new rotation if amount of rotations <3			
			AddRandomRotation ();
			break;
		case 1://Chance to remove a rotation if amount of rotations >0
			RemoveRandomRotation();
			break;
		case 2: //Chance to change an existing rotation if amount of rotations >0
			RemoveRandomRotation ();
			AddRandomRotation ();
			break;
		default:
			break;
		}
	}

	public void ApplyRotations(){
		packet.ResetRotation ();
		for(int i = 0; i<rotations.Count; i++){
			rotations[i].Apply(packet);
		}
		packet.AppliedRotation = this;
	}
	public void DeapplyRotations(){
		for (int i = rotations.Count-1; i>=0;i--) {
			rotations [i].Apply (packet);
		}
	}
}
