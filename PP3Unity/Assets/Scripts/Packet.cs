﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;
using System.Collections.Generic;
using System.Threading;

public class Packet : Cuboid
 {        
	public string Name { get; private set;}		
	public IPacketDepositStyle DepositStyle{ get; set;}

	public int ID{ get; set;}
	public static int nextID = -1;

	public List<Material> materialList;

    public void Setup()
    {       
		ID = Interlocked.Increment (ref nextID);
		Name = "P" + ID;
		gameObject.name = Name;
		PickRandomDepositStyle();
		ApplyRandomMaterial ();
    }

	public Packet(Packet packet){
		this.Name = packet.Name;
		this.DepositStyle = packet.DepositStyle;
	}


	public void ApplyRandomMaterial(){
		gameObject.GetComponent<Renderer> ().material = materialList [Random.Range (0, materialList.Count)];
	}

	public void RandomizePacketSize(float minX,float maxX,float minY,float maxY,float minZ,float maxZ){
		transform.localScale = new Vector3 (Random.Range (minX, maxX), Random.Range (minY, maxY), Random.Range (minZ, maxZ));
	}

	public void SetPacketSize(Vector3 v3){
		transform.localScale = v3;
	}

	public Vector3 GetBottomLeftRearCorner(){
		return transform.position + new Vector3(-transform.localScale.x/2,-transform.localScale.y/2 + gap,-transform.localScale.z/2);
	}
	public Vector3 GetBottomLeftFrontCorner(){
		return transform.position + new Vector3(+transform.localScale.x/2,-transform.localScale.y/2 + gap,-transform.localScale.z/2);
	}
	public Vector3 GetBottomRightRearCorner(){
		return transform.position + new Vector3(-transform.localScale.x/2,-transform.localScale.y/2 + gap,+transform.localScale.z/2);
	}
	public Vector3 GetBottomRightFrontCorner(){
		return transform.position + new Vector3(+transform.localScale.x/2,-transform.localScale.y/2 + gap,+transform.localScale.z/2);
	}

	public void ApplyDepositStyle(IPacketDepositStyle packetDepositStyle){
		this.DepositStyle = packetDepositStyle;
	}

	//Evolution
	public void PickRandomDepositStyle(){

		int decider = Random.Range(0,2);
		switch (decider) {
		case 0:
			DepositStyle = new FrontUp (this);
			return;
		case 1:
			DepositStyle = new UpFront (this);
			return;
		case 2:
			DepositStyle = new RightFrontUp (this);
			return;
		case 3:
			DepositStyle = new RightUpFront (this);
			return;
		case 4:
			DepositStyle = new FrontRightUp (this);
			return;
		case 5:
			DepositStyle = new UpRightFront (this);
			return;
		default:
			DepositStyle = new FrontUp (this);
			return;
		}
	}


}
