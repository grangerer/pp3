﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpFront : PacketDepositStyle, IPacketDepositStyle {

	public UpFront(Packet packet):base(packet){}


	public bool InsertPacket (Container container, ContainerInformation containerInformation){
		this.container = container.gameObject;
		RaycastHit hit;
		Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (-1, 0),out hit);
		if(NewInsertUpwards(containerInformation)){
			return true;
		}
		if (NewInsertToFrontFromLeft ()) {
			return true;
		}
		packet.transform.position = new Vector3 (5, 0);
		return false;
	}
}
