﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontRightUp : PacketDepositStyle, IPacketDepositStyle
{

	public FrontRightUp (Packet packet) : base (packet)
	{
	}
	public bool InsertPacket (Container container, ContainerInformation containerInformation){
		this.container = container.gameObject;
		RaycastHit hit;
		Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (-1, 0),out hit);
		if (NewInsertToFrontFromLeft ()) {
			return true;
		}
		if(NewInsertUpwards(containerInformation)){
			return true;
		}
		packet.transform.position = new Vector3 (5, 0);
		return false;
	}
//	public bool InsertPacket (Container container, ContainerInformation containerInformation)
//	{
//		//Debug.Log ("InsertFrontRightUp");
//		this.container = container.gameObject;
//		RaycastHit hit;
//		Physics.BoxCast (packet.transform.position, packet.GetBoxCastBox (), new Vector3 (-1, 0), out hit);
//		int rowsIndicator = 0;
//		while (hit.collider != null) {
//			if(containerInformation.Rows.Count == 0){
//				AddNewRow (containerInformation);
//			}
//			for (int i = rowsIndicator; i < containerInformation.Rows.Count; i++,rowsIndicator++) {
//				//Check if the row is wide (z-axis) enough
//				if (CalculateDistanceToRightWall (containerInformation.Rows[i].LeftZ) < packet.transform.localScale.z) {
//					break;
//				}
//				//Check if the packet fits in front of the collider
//				if (InsertToFront (containerInformation.Rows[i])) {
//					return true;
//				}			
//			}
//			//Check if it can fit into a new row
//			if (CalculateDistanceToRightWall (containerInformation.Rows[containerInformation.Rows.Count-1].LeftZ) >= packet.transform.localScale.z) {				
//				if (AddNewRow (containerInformation)) {
//					if (InsertToFront (containerInformation.Rows [containerInformation.Rows.Count - 1])) {
//						return true;
//					}	
//				}
//			}
//
//			//Check if it fits onto any row
//			for (int i = 0; i < containerInformation.Rows.Count; i++) {
//				if (InsertToFrontUpwards (containerInformation.Rows [i])) {
//					return true;
//				}
//			}
//			//Reset Packet position
//			packet.transform.position = new Vector3 (2, 0);
//			return false;
//
//		}
//		Debug.LogError ("This shouldn't happen");
//		return false;
//	}
}
