﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightFrontUp : PacketDepositStyle, IPacketDepositStyle {

	public RightFrontUp(Packet packet):base(packet){}

	public bool InsertPacket(Container container, ContainerInformation containerInformation){
		Debug.Log ("InsertRightFrontUp");
//		RaycastHit hit;
//		Physics.BoxCast (packet.transform.position, packet.GetHalfExtents (), new Vector3 (-1, 0), out hit);
//		int rowsIndicator = 0;
//		while (hit.collider != null) {
//			//Check if it can fit into a new row
//			if (AddNewRow (containerInformation)) {
//				//Insert it
//				if (InsertToFront (container.rows [container.rows.Count - 1])) {
//					return true;
//				}
//				continue;
//			}
//
//			for (int i = rowsIndicator; i < container.rows.Count; i++,rowsIndicator++) {
//				//Check if the row is wide (z-axis) enough
//				if (CalculateDistanceToRightWall (hit.collider.transform) < packet.transform.localScale.z) {
//					break;
//				}
//				//Check if the packet fits in front of the collider
//				if (InsertToFront (container.rows [i])) {
//					return true;
//				}
//				//Step: Go to initial position
//				if (InsertToFrontUpwards (container.rows [i])) {
//					return true;
//				}
//			}
//			//Destroy Packet gameobject
//			packet.transform.position = new Vector3 (2, 0);
//			return false;
//		}
//		Debug.LogError ("This shouldn't happen");
		return false;
	}
}
