﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainerInformation {

	int containerID;
	List<PacketInformation> packetInformations = new List<PacketInformation>();
	List<Row> rows;

	float unusedSurface;

	public ContainerInformation(int containerID){
		this.containerID = containerID;
		rows = new List<Row> ();
	}

	public void AddPacket(Packet packet){
		packetInformations.Add (new PacketInformation (packet));
	}

	public void SortPacketsByXPosition(){
		packetInformations.Sort ((PacketInformation x, PacketInformation y) => x.Position.x.CompareTo(y.Position.y));
	}

	public float CalculateUnusedSurface(){
		float unusedSurface = 0;
		foreach (var packet in packetInformations) {
			unusedSurface += packet.FindCorrespondingPacket().CalculateUnusedSurface();
			//Debug.Log ("Added unused surface " + packet.UnusedSurface);
		}
		return unusedSurface;
	}


	public List<Row> Rows {
		get {
			return rows;
		}
		set {
			rows = value;
		}
	}

	public float UnusedSurface {
		get {
			return unusedSurface;
		}
		set {
			unusedSurface = value;
		}
	}

	public int ContainerID {
		get {
			return containerID;
		}
		set {
			containerID = value;
		}
	}

	public List<PacketInformation> PacketInformations {
		get {
			return packetInformations;
		}
		set {
			packetInformations = value;
		}
	}
}
