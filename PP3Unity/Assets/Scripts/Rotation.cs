﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation {

	bool rotateAroundX;
	bool rotateAroundY;
	bool rotateAroundZ;

	public Rotation(bool x = false, bool y= false, bool z=false){
		rotateAroundX = x;
		rotateAroundY = y;
		rotateAroundZ = z;
	}

	public void Apply(Packet packet){
		if (rotateAroundX) {
			packet.RotateAroundX ();
		}
		if (rotateAroundY) {
			packet.RotateAroundY ();
		}
		if (rotateAroundZ) {
			packet.RotateAroundZ ();
		}
	}

}
